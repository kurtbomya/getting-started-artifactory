# Getting Started with Artifactory

[Artifactory](https://www.youtube.com/watch?v=r2_A5CPo43U&feature=youtu.be&t=24s) is a place for developers to internally store repositories of third-party *binaries* and their internally developed software.
It's inefficient to require each developer to retrieve their own programs *AND* it increases the chance of a failure! If a remote code repository goes offline, or alters their binaries, it could create problems for applications within our companies.

```mermaid
graph LR
    subgraph Internet
    AE(PyPi.org)
    BE(jcenter.bintray.com)
    CE(DockerHub)
    DE(registry.npmjs.org)
    end
    
    AE--proxy-->A
    BE--proxy-->B
    CE--proxy-->C
    DE--proxy-->D
    
    subgraph Artifactory
    A(Virtual Python Repository)
    B(Virtual Maven Repository)
    C(Virtual Docker Repository)
    D(Virtual NPM Repository)
    end
    
    A-->Z(( Workstations / Pipelines / Servers))
    B-->Z
    C-->Z
    D-->Z

```

See below for instructions on how to set up Artifactory on your preferred environment. 

# Package-Manager Specific Instructions

## Maven (for Java)
- Upload your Java dependencies to a new or existing Artifactory repository
- Edit your project's `pom.xml`
- Insert the Artifactory provided code blocks associated with the repo

```
<distributionManagement>
    <repository>
        <id>central</id>
        <name>bacon-releases</name>
        <url>http://artifactory.yourcompany.com:80/artifactory/your-repo</url>
    </repository>
</distributionManagement>
```

- Insert the Artifactory provided code block associated with the artifact

```
<dependency>
    <groupId>com.bacon.tomato</groupId>
    <artifactId>artifactName</artifactId>
    <version>x.x.x.x</version>
</dependency>
```

### Force download of artifacts:
*IN CMD:*
1. Navigate to your workspace (wherever your `pom.xml` file is)
1. `mvn eclipse:eclipse`

:warning: Do not globally define remote repositories in `settings.xml` file at `C:\Users\%USERNAME%\.m2\`. This potentially circumvents Artifactory entirely.

## NPM (for Node.JS)

If you are recieving an error that states that your request failed with the reason "*self signed certificate in certificate chain*" when running something like `npm install -g ionic cordova` in your cmd, run the following argument:  

`npm config set registry http://artifactory.yourcompany.com/artifactory/api/npm/npm-external --global`  

Now run `npm install -g ionic cordova` - or whatever you were installing, again.

**Mac OSX - Per User**

This document assumes a few things:

*  You've installed Node & NPM with [NVM](https://github.com/creationix/nvm)

`npm config set registry http://artifactory.yourcompany.com/artifactory/api/npm/npm-external`

:notebook: You should never have to install via the -g method NOR use sudo.

`npm install ionic`

## Gem (for Ruby)

Add the artifactory mirror and remove the default repository definition

```
gem source -a http://artifactory.yourcompany.com/artifactory/api/gems/rubygems/
gem source -r https://rubygems.org/
```

## PyPi (for Python)

:notebook: Instead of the PyPi repository, you might want to reference a **virtual repository** instead. A **virtual repository** allows you to put both external code, like that from *PyPi*, and internal code, such as an *internal Python solution*, into a single python artifact repository. To reference this, you'd replace the **index-url** with the virtual repostory you'd like to add. For example: `https://artifactory.yourcompany.com/artifactory/<repo-name>`


### Microsoft Windows 7 - All Users

> :notebook: **Note**: All commands should be ran in Windows' **cmd**

1. Create the needed directory with this command: `mkdir "C:\ProgramData\pip"`
1. Create & edit `pip.ini` file: `notepad C:\ProgramData\pip\pip.ini`
1. Add the following contents to this `pip.ini` file:

:page_with_curl: **pip Configuration File Contents**

    [global]
    timeout = 60
    index-url = https://artifactory.yourcompany.com/artifactory/api/pypi/pypi/simple
    
> **Done!**: :stop_sign: You're done! Your pip installation on Windows can now reference packages from artifactory.


### Mac OSX - All Users
1. Create a `pip` directory

    ```
    mkdir "/Library/Application Support/pip"
    ```
1. Create a blank file at `/Library/Application\ Support/pip/pip.conf`
1. Populate the aforementioned `pip.conf` file with these lines:

:page_with_curl: **pip Configuration File Contents**
```
[global]
timeout = 60 
index-url = https://artifactory.yourcompany.com/artifactory/api/pypi/pypi/simple
```


